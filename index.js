const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => res.send('SpaceStocc NodeJS Express Server'))

app.get('/apiStatus', (req, res) => res.send('OK'))

app.get('/currentNews', (req, res) => res.send(`[{"time": "2019-10-06T15:34:15", "title": "Test 2", "content": "Test content 2"},{"time": "2019-10-06T14:53:05", "title": "Test", "content": "Test content"}]`))

app.get('/stocks', (req, res) => res.send('{"BB": {"name": "BB Inc", "description": "bb company yes haha", "price": "69"}, "APPL": {"name": "Apple Inc.", "description": "Apple, Inc. engages in designing, manufacturing, and marketing of mobile communication, media devices, personal computers, and portable digital music players.", "price": "1"}}'))

app.listen(port, () => console.log(`SpaceStocc NodeJS Express Server On Port ${port}!`))